\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{Game Description}{3}{0}{1}
\beamer@sectionintoc {2}{Lowest payoff outcome}{9}{0}{2}
\beamer@sectionintoc {3}{Nash equilibrium}{11}{0}{3}
\beamer@sectionintoc {4}{Socially optimal behavior}{14}{0}{4}
\beamer@subsectionintoc {4}{1}{The mathematical model}{15}{0}{4}
\beamer@subsectionintoc {4}{2}{The computational model}{18}{0}{4}
\beamer@subsectionintoc {4}{3}{Regression Analysis}{20}{0}{4}
