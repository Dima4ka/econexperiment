\section{Introduction}

	In this paper, we present a dynamic, voluntary contribution mechanism, public good game and solve it for the lowest payoff, Nash equilibrium and socially optimal outcomes.
	While lowest payoff and Nash equilibrium cases are relatively straightforward, we concentrate on the socially optimal outcome.
	We build a mathematical model, simplify it, then use computational methods and regression analysis to derive analytical solution.  

\section{The Game}

	The game occurs among groups of 4 people and consists of 10 periods.
	Each period has two stages: an investment stage and a contribution stage.
	At the start of every period, all players receive endowments of 10.
	
	\subsection{Investment Stage}
	
		This game incorporates endogenous determination of contribution productivity and dynamic links between periods.
		These two aspects are incorporated through the investment stage of each period where players have the opportunity to increase
		their contribution productivity from the starting value of $0.30$ (the low used by Isaac and Walker (1988)).
		First, players vote to determine the amount each player in the group will invest in increasing contribution productivity by choosing a whole number between zero and ten, inclusive.
		A median voter rule is applied and the group's investment is the average of the two middle votes.
		Then, contribution productivity increases by $0.01$ multiplied by the investment.
		For example, in the first period where contribution productivity equals $0.30$, if players vote $1$, $3$, $5$, and $6$, the investment will be $4$.
		Therefore, all four group members invest $4$ and have a remaining amount of $6$ left after the investment.
		Contribution productivity then equals $0.34$.
		Players vote on an investment and invest in contribution productivity every period, and the amount builds throughout the ten periods.

		\[
			\text{Contribution productivity} = M_t = M_{t-1} + 0.01 \cdot I_t
		\]
		\[
			\text{for } t = [1..10]
		\]
		\[
			M_0 = 0.3
		\]
	
	\subsection{Contribution Stage}
	
		Following the investment stage is the contribution stage where players decide how to allocate their remaining money
		between a private consumption and public good, similar to a standard public good game.
		If the investment had been $10$, then the players must contribute zero to the public good because they have no money left in the period.
		The sum of the group members' contributions is multiplied by the new $M_t$ each period and this amount,
		in addition to any money the player has remaining after the investment and contribution stage, is the player's payoff for that period
		(note that every player benefits from the sum of the group contributions multiplied by the $M_t$ regardless of whether or not they contribute).
		Thus, each player's payoff for each period is:

		\[
			\pi_{it} = \omega - I_t - c_{it} + M_t \sum c_{jt}
		\]
		
		where:
			\begin{itemize}
				\item
					$\pi_{i}$ is the individual's payoff for the period
				\item
					$\omega$ is the individual's endowment
				\item
					$I$ is the investment for the period
				\item
					$c_i$ is the individual's contribution to the public good
				\item
					$M$ is the contribution productivity
				\item
					$c_j$ is the other members' contributions to the public good
				\item
					$t$ is the period
			\end{itemize}

		Here is an example ($M_0 = 0.3$):
		\begin{center}
			\begin{tabular}{ | c | c | c | c | c | c | c |}
				\hline
				Players	& $\omega$	& $I_t$						& $M_t$					& $C_{it}$	& $M_t \sum c_{jt}$			& $\pi_{it}$	\\ \hline
				1		& $10$	& \multirow{4}{*}{$3$}	& \multirow{4}{*}{0.33}	& $7$		& \multirow{4}{*}{$4.95$}	& $4.95$		\\ 
				2		& $10$	& 							&						& $5$		&							& $6.95$		\\ 
				3		& $10$	& 							&						& $3$		&							& $8.95$		\\ 
				4		& $10$	& 							&						& $0$		&							& $11.95$		\\ \hline
			\end{tabular}
		\end{center}
		
		In this case, the players' endowments are $10$.
		They collectively decide to invest $3$.
		Their multiplier gets increased by $0.01 \cdot 3 = 0.03$ and is equal to $0.33$.
		Players individually decide to contribute $7$ (all that is left), $5$, $3$ and nothing.
		The payoffs are $4.95$, $6.95$, $8.95$ and $11.95$.

\section{Potential Outcomes}

	In this section we determine:

	\begin{itemize}
		\item
			\textbf{The lowest payoff outcome.}
			How would the players act to get the lowest possible payoffs?
			What are the lowest possible payoffs?
		\item
			\textbf{The Nash equilibrium.}
			What would happen if each player acted in his own interest?
		\item 
			\textbf{The Socially optimal outcome.}
			How should the players act so that the sum of payoffs is maximized?
			What is this sum of payoffs?
	\end{itemize}
		
	\subsection{Lowest payoff outcome}
	
		The lowest payoff one could get is $0$. 
		This occurs if the group invests everything in every period and never contributes anything. 
		Payoffs are $0$ in every period and $0$ at the end of $10$ periods.
		
	\subsection{Nash equilibrium}
		
		The definition of Nash equilibrium is:
	
		\begin{displayquote}
			\textit{
				A Nash equilibrium, also called strategic equilibrium, is a list of strategies, one for each player, 
				which has the property that no player can unilaterally change his strategy and get a better payoff.
			} \\
			Turocy and von Stengel (2001)
		\end{displayquote}
		
		Let us solve the problem by backward induction.
		
		Let us think of the last period.
		How does each player maximize his payoff?
		Each player knows that if he contributes anything he reduces his payoff.
		Therefore, the player decides not to contribute.
		
		All players follow the same (equilibrium) strategy.
		If nobody contributes, then it is optimal for no one to invest.
		
		If nobody invests or contributes, then everyone is left with his $\omega$ (endowment).
		
		By induction, we can show this occurs for all previous periods up to the first one.
		
		As a result, the Nash equilibrium is for everyone to keep his money.
		Each person's payoff is $10 \cdot 10 = 100$.
			
	\subsection{Socially optimal behavior}
	
		\subsubsection{The mathematical model}
	
			To approach this problem we have to build a mathematical model.
			Thinking of a single period we can define a function $f$ of investment $I$ and contribution $C$ that returns a payoff.
			Let us define this function for the period after the $t_\text{th}$ period.
			
			\[
				f(I, C) = [M_t \cdot 4 \cdot C] + [\omega - C - I]
			\]
			\[
				M_t = M_{t-1} \cdot (1 + 0.01 \cdot I)
			\]
			\[
				M_0 = 0.3
			\]

			$M_t \cdot 4 \cdot C$ is payoff and $\omega - C - I$ is the amount left after both stages.
			
			Since every $\$1$ contributed will give at least $0.3 \cdot 4 = \$1.2$ back,
			we see that keeping money in the contribution is not socially optimal and we can state an assumption:
			
			\begin{displayquote}
				\emph{
					The optimal result requires contributing all that is left after the investment.
				} 
			\end{displayquote} 
		
			Now, the term $\omega - C - I$ equals $0$, so we see that $I = \omega - C$, which means that we can eliminate one of the two variables - $C$ or $I$.
			Let us instead introduce a new variable $p$ as a fraction of endowment which player invests.

			Now $I = p \cdot \omega$ and $C = (1-p) \cdot \omega$.
			Let us redefine our function
			
			\[
				f(p) = M_t \cdot 4 \cdot \omega \cdot (1 - p)
			\]
			\[
				M_t = M_{t-1} \cdot (1 + 0.01 \cdot \omega \cdot p)
			\]
			\[
				M_0 = 0.3
			\]
			where:
			\begin{itemize}
				\item
					$p$ is the \emph{proportion} of investment
				\item 
					$\omega$ is the endowment ($10$)
				\item
					$M_t$ is the $t_\text{th}$ multiplier
			\end{itemize}
		
			From now, let us solve it specifically for our case, when endowment is $10$.
			\[
				f(p) = 40 \cdot M_t \cdot (1 - p)
			\]
			\[
				M_t = M_{t-1} \cdot (1 + \frac{p}{10})
			\]
			\[
				M_0 = 0.3
			\]
			where:
			\begin{itemize}
				\item
					$p$ is the \emph{proportion} of investment
				\item
					$M_t$ is the $t_\text{th}$ multiplier
			\end{itemize}
			
			This is a recursive function that is difficult to solve analytically.
	
		\subsubsection{The computational model}
		
			Having the mathematical model, we can make use of a computer to solve it numerically,
			and then use regression analysis to derive the analytical solution.
		
			First, let us approximate how much time it would take for the computer to execute this simulation.
			
			If we ran the simulation for 10 periods with a step ($\Delta p$) that is at least as small as $0.1$,
			the time complexity of the algorithm would be $O(n^a)$, which is $\approx 10^{10} \cdot c$ computations.
			It would take months to run the simulation on a usual PC, so we need to improve the formula.
			
		
			Let us make a second assumption:
			\begin{displayquote}
				\emph{
					The optimal solution requires that players first only invest then only contribute.
					In other words, $p=1.0$ for some number of periods, then $p \in [0.0, 1.0]$ for one period,
					and finally $p = 0.0$ for the rest of the game.
				}
			\end{displayquote}
			
			This assumption is based on the fact that the value of an investment declines and the value of a contribution grows with time.
			
			This assumption reduces the complexity of the algorithm to linear - $O(n)$.
			
			Let us define two functions (see Appendix A) that compute payoff for each possible $p$.
			This program runs the game $10$ periods for all possible values of $p$ taking into account the second assumption.
			For each such run it returns a payoff.
			The result of the computation is in the Appendix B.
			
			Although this simulation immediatelly gives us the value of $p$ where payoff is maximized, we are interested in a generic solution.
			Let us plot our function.
			According to the Fig.~\ref{fig:bifurcation} the resulting function has a global maximum and is probably quadratic with $a < 0$.
			
			\begin{figure}
				\begin{center}
					\includegraphics[width=8.4cm]{resources/plot.png}	% The printed column width is 8.4 cm.
					\caption{Function of payoff depending on time when a player switches to contribution - $f(p)$} 
					\label{fig:bifurcation}
				\end{center}
			\end{figure}			 
		
		\subsubsection{Regression Analysis}
		
			The last step is to try to "guess" the formula for the data. We enter the data in the regression tool (\emph{http://www.xuru.org/rt/PR.asp}),
			give it a guess of mathematical model (in our case it is a quadratic formula $ax^2 + bx + c$) and run it.
			The tool would give us the formula with numeric coefficients and error value.
			In our case we got a quadratic formula with the error value equal to $0$ which means perfect fit.
			Now, let us "guess" generic formula so that it fits numeric one.
			
			\[
				f(x) = 400 \cdot \left[ - m \cdot \omega \cdot x^2 + (m \cdot \omega \cdot T - M_0) \cdot x + M_0 \cdot T \right]
			\]
			\[
				x_\text{max} = \frac{T}{2} - \frac{M_0}{2 \cdot m \cdot \omega}
			\]
			\[
				f_\text{max} = f(x_\text{max}) = f \left(\frac{T}{2} - \frac{M_0}{2 \cdot m \cdot \omega} \right)
			\]
			where:
			\begin{itemize}
				\item
					$m$ is the increase in multiplier ($0.01$)
				\item
					$T$ is the number of periods
				\item
					$x$ is the stage when players switch to contributing.
					The number before the decimal point defines a period.
					The number before the decimal point defines an investment in that period.
			\end{itemize}
			
			The computation and regression analysis were conducted for different values of $m$, $\omega$, $M_t$, $T$ and $x$ to demonstrate the robustness of the formula.
			
			As a final step, with a generic formula we can compute the actual outcome by using our specific initial values.
			
			\[
				f(x) = -0.1 x^2 + 0.7 x + 3
			\]
			\[
				x_{\text{optimal}} = 3.5
			\]
			
			which indicates investment until the $4_\text{th}$ period and in that period investment of $0.5$
			
			\[
				f_{\text{optimal}} = f(x_{\text{optimal}}) = 169
			\]
			
			which implies the payoff of $169$.
			
			The maximum payoff for each player in the group of $4$ is $169$.
			To achieve this, the players must invest everything until period $4$,
			then invest half of their endowments, and contribute everything afterwards.

\section{Conclusion}

	To summarize the analysis, let us define the outcomes.
	
	Lowest payoff outcome occurs if players contribute everything every period.
	Their individual and group payoffs are $0$.
	
	The Nash equilibrium occurs if no one invests or contributes.
	Player's individual payoff is $100$.
	
	Socially optimal outcome occurs if players invest everything until the fourth period,
	then invest half of their endowments in that period, and contribute everything afterwards.
	Player's individual payoff is $169$.