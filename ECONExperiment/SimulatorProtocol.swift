//
//  SimulatorProtocol.swift
//  ECONExperiment
//
//  Created by Dmytro Bogatov on 11/5/15.
//  Copyright © 2015 Dmytro Bogatov. All rights reserved.
//

import Foundation

protocol SimulatorProtocol {
	
	func startSimulation() -> SimulatorProtocol;
	
	func printHighestRevenueData() -> String;
	
	func getHighestRevenueData() -> (revenue: Double, investment: Double, contribution: Double);
	
	var initialCapital : Int { get }
	
}