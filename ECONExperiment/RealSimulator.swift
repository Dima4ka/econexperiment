//
//  RealSimulator.swift
//  ECONExperiment
//
//  Created by Dmytro Bogatov on 11/5/15.
//  Copyright © 2015 Dmytro Bogatov. All rights reserved.
//

import Foundation

class RealSimulator : Simulator {
	init(userContribution : Double, userInvestment : Double) {
		super.init(initialCapital: 10, initialMultiplier: 0.3, rounds: 10, teamSize: 4);
				
		assert(userInvestment + userContribution <= Double(initialCapital), "Wrong user parameters");
		
		self.userContribution = userContribution;
		self.userInvestment = userInvestment;
	}
}