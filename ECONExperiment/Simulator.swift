//
//  Simulator.swift
//  ECONExperiment
//
//  Created by Dmytro Bogatov on 11/5/15.
//  Copyright © 2015 Dmytro Bogatov. All rights reserved.
//

import Foundation

class Simulator : SimulatorProtocol {
    
	var initialCapital : Int;
	var initialMultiplier : Double;
	var rounds : Int;
	var teamSize : Int;
	
	var cummulativeRevenue : Double = 0.0;
    
	var userContribution : Double = 0.0;
	var userInvestment : Double = 0.0;
	
	init(initialCapital : Int, initialMultiplier : Double, rounds : Int, teamSize : Int) {
		self.initialCapital = initialCapital;
		self.initialMultiplier = initialMultiplier;
		self.rounds = rounds;
		self.teamSize = teamSize;
	}
	
	func startSimulation() -> SimulatorProtocol {
		//print(self.printInitialMessage());
		
		for _ in 1...self.rounds {
			self.initialMultiplier += self.userInvestment * Double(self.teamSize) * 0.01;
			self.cummulativeRevenue += self.userContribution * Double(self.teamSize) * self.initialMultiplier;
			self.cummulativeRevenue += Double(self.initialCapital) - (self.userInvestment + self.userContribution);
		}
		
		
		return self;
		//print(printHighestRevenueData());
	}
	
	func printInitialMessage() -> String {
		return "Simulator for parameters CAPITAL=\(self.initialCapital), MULTIPLIER=\(self.initialMultiplier), ROUNDS=\(self.rounds), TEAMSIZE=\(self.teamSize) has started";
	}
	
	func getHighestRevenueData() -> (revenue: Double, investment: Double, contribution: Double) {
		return (self.cummulativeRevenue, self.userInvestment, self.userContribution);
	}
	
	func printHighestRevenueData() -> String {
		return "Revenue is \(self.cummulativeRevenue) for investement \(self.userInvestment) and contribution \(self.userContribution)";
	}
	
}